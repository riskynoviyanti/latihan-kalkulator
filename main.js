// untuk menampilkan hasil dari kalkulator
var tampil = document.formAngka.isiText;

// untuk mengidentifikasi angka dan operator
function ins(nun){
    tampil.value += nun;
}

// untuk menghapus semua angka
function hapusAll(){
    tampil.value ="";
}

// untuk menghapus angka tetapi tidak terhapus semuanya hanya satu angka yang terhapus
function bcksp(){
    tampil.value = tampil.value.substr(0,tampil.value.length-1);
}

// untuk menampilkan hasil perhitungan yang kita hitung
function samaDengan(){
    tampil.value = eval(tampil.value);
}